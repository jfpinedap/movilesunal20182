package com.example.jfpinedap.androidtictactoe;



import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuInflater;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.widget.Toast;
import android.content.Context;


public class AndroidTicTacToeActivity extends AppCompatActivity {

    // Constants which will be used to identify the two dialog boxes
    static final int DIALOG_DIFFICULTY_ID = 0;
    static final int DIALOG_EXIT_GAME_ID = 1;
    static final int DIALOG_ABOUT_ID = 2;

    // Represents the internal state of the game
    private TicTacToeGame mGame;

    private SharedPreferences mPrefs;

    // Various text displayed
    private TextView mInfoTextView;
    private TextView mHumanScoreTextView;
    private TextView mComputerScoreTextView;
    private TextView mTieScoreTextView;

    // Various game score records
    private int humanScore;
    private int computerScore;
    private int tieScore;

    // Who play at first? player or computer
    private char mTurn = TicTacToeGame.HUMAN_PLAYER;

    // Locks the board game
    private boolean mGameOver;

    private BoardView mBoardView;

    private boolean setMove_;

    // MediaPlayer variables for the sound effects
    MediaPlayer mHumanMediaPlayer;
    MediaPlayer mComputerMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_tic_tac_toe);
        mInfoTextView = (TextView) findViewById(R.id.information);
        mHumanScoreTextView = (TextView) findViewById(R.id.player_score);
        mComputerScoreTextView = (TextView) findViewById(R.id.computer_score);
        mTieScoreTextView = (TextView) findViewById(R.id.tie_score);

        mGame = new TicTacToeGame();
        mBoardView = (BoardView) findViewById(R.id.board);
        mBoardView.setGame(mGame);

        // Listen for touches on the board
        mBoardView.setOnTouchListener(mTouchListener);

        mPrefs = getSharedPreferences("ttt_prefs", MODE_PRIVATE);

        // Restore the scores
        humanScore = mPrefs.getInt("mHumanWins", 0);
        computerScore = mPrefs.getInt("mComputerWins", 0);
        tieScore = mPrefs.getInt("mTies", 0);

        // Restore the difficulty level saved on mPrefs
        mGame.setDifficultyLevel((TicTacToeGame.DifficultyLevel.values()[mPrefs.getInt("difficultyLevel",
                TicTacToeGame.DifficultyLevel.Easy.ordinal())]));


        //startNewGame();

        if (savedInstanceState == null) {
            startNewGame();
        }
//        else {
//
//            // Restore the game's state
//            mGame.setBoardState(savedInstanceState.getCharArray("board"));
//            mGameOver = savedInstanceState.getBoolean("mGameOver");
//            mInfoTextView.setText(savedInstanceState.getCharSequence("mInfoTextView"));
//            humanScore = savedInstanceState.getInt("humanScore");
//            computerScore = savedInstanceState.getInt("computerScore");
//            tieScore = savedInstanceState.getInt("tieScore");
//            mTurn = savedInstanceState.getChar("mTurn");
//        }
//
//        displayScores();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
/*        super.onCreateOptionsMenu(menu);
        menu.add("New Game");
        menu.add("Restore Scores");
        return true;*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.new_game:
                if (mTurn == TicTacToeGame.HUMAN_PLAYER) {
                    mTurn = TicTacToeGame.COMPUTER_PLAYER;
                } else {
                    mTurn = TicTacToeGame.HUMAN_PLAYER;
                }
                startNewGame();
                return true;
            case R.id.difficulty:
                showDialog(DIALOG_DIFFICULTY_ID);
                return true;
            case R.id.reset_scores:
                resetGame();
                return true;
            case R.id.about:
                showDialog(DIALOG_ABOUT_ID);
                return true;
            case R.id.exit_game:
                showDialog(DIALOG_EXIT_GAME_ID);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        switch(id) {
            case DIALOG_DIFFICULTY_ID:
                builder.setTitle(R.string.difficulty_choose);
                final CharSequence[] levels = {
                        getResources().getString(R.string.difficulty_easy),
                        getResources().getString(R.string.difficulty_harder),
                        getResources().getString(R.string.difficulty_expert)};

                // TODO: Set selected, an integer (0 to n-1), for the Difficulty dialog.
                // selected is the radio button that should be selected.

                int selected;
                switch (mGame.getDifficultyLevel()){
                    case Expert:
                        selected = 2;
                        break;
                    case Harder:
                        selected = 1;
                        break;
                    case Easy:
                        selected = 0;
                        break;
                    default:
                        selected = 2;
                        break;
                }

                builder.setSingleChoiceItems(levels, selected,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                dialog.dismiss();

                                // Close dialog
                                // TODO: Set the diff level of mGame based on which item was selected.
                                // Display the selected difficulty level

                                switch (item) {
                                    case 0:
                                        mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Easy);
                                        break;
                                    case 1:
                                        mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Harder);
                                        break;
                                    case 2:
                                        mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Expert);
                                        break;
                                }

                                Toast.makeText(getApplicationContext(), levels[item],
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                dialog = builder.create();
                break;

            case DIALOG_EXIT_GAME_ID:
                // Create the quit confirmation dialog
                builder.setMessage(R.string.exit_game_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AndroidTicTacToeActivity.this.finish();
                            }
                        })
                        .setNegativeButton(R.string.no, null);
                dialog = builder.create();
                break;

            case DIALOG_ABOUT_ID:
                Context context = getApplicationContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.about_dialog, null);
                builder.setView(layout);
                builder.setPositiveButton("OK", null);
                dialog = builder.create();
                break;
        }
        return dialog;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHumanMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sword);
        mComputerMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.swish);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHumanMediaPlayer.release();
        mComputerMediaPlayer.release();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharArray("board", mGame.getBoardState());
        outState.putBoolean("mGameOver", mGameOver);
        outState.putCharSequence("mInfoTextView", mInfoTextView.getText());
        outState.putInt("humanScore", Integer.valueOf(humanScore));
        outState.putInt("computerScore", Integer.valueOf(computerScore));
        outState.putInt("tieScore", Integer.valueOf(tieScore));
        outState.putChar("mTurn", mTurn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mGame.setBoardState(savedInstanceState.getCharArray("board"));
        mGameOver = savedInstanceState.getBoolean("mGameOver");
        mInfoTextView.setText(savedInstanceState.getCharSequence("mInfoTextView"));
        humanScore = savedInstanceState.getInt("humanScore");
        computerScore = savedInstanceState.getInt("computerScore");
        tieScore = savedInstanceState.getInt("tieScore");
        mTurn = savedInstanceState.getChar("mTurn");
        displayScores();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Save the current scores
        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt("mHumanWins", humanScore);
        ed.putInt("mComputerWins", computerScore);
        ed.putInt("mTies", tieScore);
        ed.putInt("difficultyLevel", mGame.getDifficultyLevel().ordinal());
        ed.commit();
    }

    private void displayScores() {
        mTieScoreTextView.setText(' '+Integer.toString(tieScore));
        mHumanScoreTextView.setText(' '+Integer.toString(humanScore));
        mComputerScoreTextView.setText(Integer.toString(computerScore));
    }

    // Set up the game board.
    private void resetGame() {
        mGame.clearBoard();

        mBoardView.invalidate(); // Redraw the board

        // Various game score records
        humanScore = 0;
        computerScore = 0;
        tieScore = 0;

        displayScores();


        mInfoTextView.setText(R.string.first_human);
        mGameOver = false;


        Toast.makeText(getApplicationContext(), "Reset scores",
                Toast.LENGTH_SHORT).show();
    }

    // Set up the game board.
    private void startNewGame() {
        mGame.clearBoard();

        mBoardView.invalidate(); // Redraw the board

        if(mTurn == TicTacToeGame.HUMAN_PLAYER){

            mInfoTextView.setText(R.string.first_human);
        }
        else{
            // Computer goes first
            mInfoTextView.setText(R.string.turn_computer);
            int move = mGame.getComputerMove();
            setMove(TicTacToeGame.COMPUTER_PLAYER, move);
        }

        mGameOver = false;

        displayScores();

        Toast.makeText(getApplicationContext(), "New Game",
                Toast.LENGTH_SHORT).show();
        //mInfoTextView.setText("You go first.");
        //mInfoTextView.setText(R.string.first_human);
    }// End of startNewGame

    // Listen for touches on the board
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {

            // Determine which cell was touched
            int col = (int) event.getX() / mBoardView.getBoardCellWidth();
            int row = (int) event.getY() / mBoardView.getBoardCellHeight();
            int pos = row * 3 + col;
            if (!mGameOver && setMove(TicTacToeGame.HUMAN_PLAYER, pos)) {

                    // If no winner yet, let the computer make a move
                    int winner = mGame.checkForWinner();
                    if (winner == 0) {
                        // Set computer turn
                        mTurn = TicTacToeGame.COMPUTER_PLAYER;

                        mInfoTextView.setText(R.string.turn_computer);
                        int move = mGame.getComputerMove();
                        setMove(TicTacToeGame.COMPUTER_PLAYER, move);
                    } else {
                        publishResultGame(winner);
                    }
                }
                // So we aren't notified of continued events when finger is moved
                return false;
        }
    };

    private boolean setMove(char player, int location) {

        if (player == TicTacToeGame.COMPUTER_PLAYER) {
            Handler handler = new Handler();
            final char player_ = player;
            final int location_ = location;
            handler.postDelayed(new Runnable() {
                public void run() {
                    // Play the sound effect
                    mComputerMediaPlayer.start();
                    if (mGame.setMove(player_, location_)) {
                        mBoardView.invalidate();

                        // If no winner yet, let the computer make a move
                        int winner = mGame.checkForWinner();
                        if (winner == 0) {
                            // Set human turn
                            mTurn = TicTacToeGame.HUMAN_PLAYER;

                            mInfoTextView.setText(R.string.turn_human);
                        } else {
                            publishResultGame(winner);
                        }

                        // Redraw the board
                        setMove_ = true;
                    }
                    setMove_ = false;
                }
            }, 1000);
            return setMove_;
        }
        else if (mTurn == TicTacToeGame.HUMAN_PLAYER){
            // Play the sound effect
            mHumanMediaPlayer.start();
            if (mGame.setMove(player, location)) {
                mBoardView.invalidate();

                // Redraw the board
                return true;
            }
            return false;
        }
        return false;
    }

    private void publishResultGame(int winner){

        if (winner == 0)
            mInfoTextView.setText(R.string.turn_human);
        else if (winner == 1) {
            tieScore++;
            mTieScoreTextView.setText(' ' + Integer.toString(tieScore));
            mInfoTextView.setText(R.string.result_tie);
            mGameOver = true;
        } else if (winner == 2) {
            humanScore++;
            mHumanScoreTextView.setText(' ' + Integer.toString(humanScore));
            mInfoTextView.setText(R.string.result_human_wins);
            mGameOver = true;
        } else {
            computerScore++;
            mComputerScoreTextView.setText(Integer.toString(computerScore));
            mInfoTextView.setText(R.string.result_computer_wins);
            mGameOver = true;
        }
    }
}
