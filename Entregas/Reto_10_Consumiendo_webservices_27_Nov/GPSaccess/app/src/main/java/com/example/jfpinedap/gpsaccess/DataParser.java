package com.example.jfpinedap.gpsaccess;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataParser {
    public List<HashMap<String, String>> parse(String jsonData) {
        jsonData = "{\"results\":" + jsonData + "}";
        JSONArray jsonArray = null;
        JSONObject jsonObject;
        try {
            Log.d("Places", "parse");
            jsonObject = new JSONObject((String) jsonData);
            jsonArray = jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            Log.d("Places", "parse error");
            e.printStackTrace();
        }
        return getPlaces(jsonArray);
    }
    private List<HashMap<String, String>> getPlaces(JSONArray jsonArray) {
        int placesCount = jsonArray.length();
        List<HashMap<String, String>> placesList = new ArrayList<>();
        HashMap<String, String> placeMap = null;
        Log.d("Places", "getPlaces");
        for (int i = 0; i < placesCount; i++) {
            try {
                placeMap = getPlace((JSONObject) jsonArray.get(i));
                placesList.add(placeMap);
                Log.d("Places", "Adding places");
            } catch (JSONException e) {
                Log.d("Places", "Error in Adding places");
                e.printStackTrace();
            }
        }
        return placesList;
    }
    private HashMap<String, String> getPlace(JSONObject googlePlaceJson) {
        HashMap<String, String> wifiZoneMap = new HashMap<String, String>();
        String id_wifi_zone = "0";
        String nombre_zona_wifi = "-NA-";
        String latitud = "0";
        String longitud = "0";
        String departamento = "-NA-";
        String direccion = "-NA-";
        String municipio = "-NA-";

        Log.d("getPlace", "Entered");
        try {
            if (!googlePlaceJson.isNull("no")) {
                id_wifi_zone = googlePlaceJson.getString("no");
            }
            if (!googlePlaceJson.isNull("nombre_zona_wifi")) {
                nombre_zona_wifi = googlePlaceJson.getString("nombre_zona_wifi");
            }
            if (!googlePlaceJson.isNull("latitud")) {
                latitud = googlePlaceJson.getString("latitud");
            }
            if (!googlePlaceJson.isNull("longitud")) {
                longitud = googlePlaceJson.getString("longitud");
            }
            if (!googlePlaceJson.isNull("departamento")) {
                departamento = googlePlaceJson.getString("departamento");
            }
            if (!googlePlaceJson.isNull("direccion")) {
                direccion = googlePlaceJson.getString("direccion");
            }
            if (!googlePlaceJson.isNull("municipio")) {
                municipio = googlePlaceJson.getString("municipio");
            }
            wifiZoneMap.put("id_wifi_zone", id_wifi_zone);
            wifiZoneMap.put("nombre_zona_wifi", nombre_zona_wifi);
            wifiZoneMap.put("latitud", latitud);
            wifiZoneMap.put("longitud", longitud);
            wifiZoneMap.put("departamento", departamento);
            wifiZoneMap.put("direccion", direccion);
            wifiZoneMap.put("municipio", municipio);
            Log.d("getPlace", "Putting Places");
        } catch (JSONException e) {
            Log.d("getPlace", "Error");
            e.printStackTrace();
        }
        return wifiZoneMap;
    }
}