package com.example.jfpinedap.gpsaccess;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class GetWifiZoneData extends AsyncTask<Object, String, String> {


    private double minLatitud;
    private double maxLatitud;
    private double minLongitud;
    private double maxLongitud;
    private double centerLatitude;
    private double centerLongitude;
    private List<HashMap<String, String>> wifiZone;

    public GetWifiZoneData (Context context){

    }

    public double getCenterLatitude() {
        return centerLatitude;
    }

    public double getCenterLongitude() {
        return centerLongitude;
    }

    public List<HashMap<String, String>> getWifiZone() {
        return wifiZone;
    }


    String wifiZoneData;
    GoogleMap mMap;
    String url;
    @Override
    protected String doInBackground(Object... params) {
        try {
            Log.d("GetNearbyBanksData", "doInBackground entered");
            mMap = (GoogleMap) params[0];
            url = (String) params[1];
            UrlConnection urlConnection = new UrlConnection();
            wifiZoneData = urlConnection.readUrl(url);
            Log.d("SocrataWifiZoneReadTask", wifiZoneData);
            Log.d("SocrataWifiZoneReadTask", "doInBackground Exit");
        } catch (Exception e) {
            Log.d("SocrataWifiZoneReadTask", e.toString());
        }
        return wifiZoneData;
    }
    @Override
    protected void onPostExecute(String result) {
        Log.d("SocrataWifiZoneReadTask", "onPostExecute Entered");
        wifiZone = null;
        DataParser dataParser = new DataParser();
        wifiZone = dataParser.parse(result);
        minLatitud = 0;
        maxLatitud = 0;
        minLongitud = 0;
        maxLongitud = 0;
        ShowWifiZone(wifiZone);
        Log.d("SocrataWifiZoneReadTask", "onPostExecute Exit");
    }
    private void ShowWifiZone(List<HashMap<String, String>> wifiZoneList) {
        for (int i = 0; i < wifiZoneList.size(); i++) {
            Log.d("onPostExecute", "Entered into showing locations");
            MarkerOptions markerOptions = new MarkerOptions();
            HashMap<String, String> wifiZone = wifiZoneList.get(i);
            float id_wifi_zone = Integer.parseInt(wifiZone.get("id_wifi_zone"));
            String nombre_zona_wifi = wifiZone.get("nombre_zona_wifi");
            double latitud = Double.parseDouble(wifiZone.get("latitud"));
            double longitud = Double.parseDouble(wifiZone.get("longitud"));
            String departamento = wifiZone.get("departamento");
            String direccion = wifiZone.get("direccion");
            String municipio = wifiZone.get("municipio");
            LatLng latLng = new LatLng(latitud, longitud);

            if (!municipio.equals("Bogotá D.C. (Anexo Transmilenio)") && id_wifi_zone != 341){
                mMap.addMarker(new MarkerOptions()
                        .zIndex(id_wifi_zone)
                        .position(latLng)
                        .title(nombre_zona_wifi)
                        .snippet("Municipio: " + municipio));

                if (minLatitud == 0 || minLatitud > latitud){
                    minLatitud = latitud;
                }

                if (maxLatitud == 0 || maxLatitud < latitud){
                    maxLatitud = latitud;
                }

                if (minLongitud == 0 || minLongitud > longitud){
                    minLongitud = longitud;
                }

                if (maxLongitud == 0 || maxLongitud < longitud){
                    maxLongitud = longitud;
                }
            }

//            mMap.addMarker(markerOptions);
//            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));


        }

        centerLatitude = minLatitud + ((maxLatitud - minLatitud)/2);
        centerLongitude = minLongitud + ((maxLongitud - minLongitud)/2);
        LatLng center = new LatLng(centerLatitude, centerLongitude);
        LatLng neCoord = new LatLng(maxLatitud, maxLongitud);
        LatLng swCoord = new LatLng(minLatitud, minLongitud);
        Log.d("neCoord", neCoord.toString());
        Log.d("swCoord", swCoord.toString());

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(neCoord)
                .include(swCoord)
                .build();

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(center));
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));

    }
}
