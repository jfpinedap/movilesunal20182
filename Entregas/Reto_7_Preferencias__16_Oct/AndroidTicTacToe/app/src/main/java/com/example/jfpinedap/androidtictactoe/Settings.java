package com.example.jfpinedap.androidtictactoe;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;


public class Settings extends PreferenceActivity {
//    public class Settings extends PreferenceActivity implements ColorPickerDialog.OnColorChangedListener{

    public static final String BOARD_COLOR = "board_color";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        final SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getBaseContext());

//        final ListPreference difficultyLevelPref = (ListPreference) findPreference("difficulty_level");
//        String difficulty = prefs.getString("difficulty_level",
//                getResources().getString(R.string.difficulty_expert));
//        difficultyLevelPref.setSummary(difficulty);
//        difficultyLevelPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//            @Override
//            public boolean onPreferenceChange(Preference preference, Object newValue) {
//                difficultyLevelPref.setSummary((CharSequence) newValue);
//
//                // Since we are handling the pref, we must save it
//                SharedPreferences.Editor ed = prefs.edit();
//                ed.putString("difficulty_level", newValue.toString());
//                ed.apply();
//                return true;
//            }
//        });

        final EditTextPreference victoryMessagePref = (EditTextPreference)
                findPreference("victory_message");
        String victoryMessage = prefs.getString("victory_message",
                getResources().getString(R.string.result_human_wins));
        victoryMessagePref.setSummary(victoryMessage);
        victoryMessagePref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                victoryMessagePref.setSummary((CharSequence) newValue);

                // Since we are handling the pref, we must save it
                SharedPreferences.Editor ed = prefs.edit();
                ed.putString("victory_message", newValue.toString());
                ed.apply();
                return true;
            }
        });

//        final Preference boardColorPref =  findPreference(BOARD_COLOR);
//        boardColorPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            @Override
//            public boolean onPreferenceClick(Preference preference) {
//                int color = prefs.getInt(BOARD_COLOR, Color.LTGRAY);
//                new ColorPickerDialog(Settings.this, Settings.this, color).show();
//                return true;
//            }
//        });

    }

//    @Override
//    public void colorChanged(int color) {
//        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(BOARD_COLOR, color).apply();
//
//    }
}
