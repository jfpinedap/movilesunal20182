package com.example.jfpinedap.enterprisedirectory;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jfpinedap.enterprisedirectory.DB.EnterpriseOperations;
import com.example.jfpinedap.enterprisedirectory.Model.Enterprise;

import java.util.List;

public class ViewCategoryEnterprise extends ListActivity {
    private static final String EXTRA_CATEGORY_SEARCH = "com.example.jfpinedap.category_search";


    private EnterpriseOperations enterpriseOps;
    List<Enterprise> enterprises;

    TextView tittleTextView;
    TextView tittleArgTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_queries);


        tittleTextView = (TextView) findViewById(R.id.text_view_all_enterprises);
        tittleArgTextView = (TextView) findViewById(R.id.text_view_all_enterprises_arg);
        tittleTextView.setText(R.string.view_category_query);

        int category = getIntent().getIntExtra(EXTRA_CATEGORY_SEARCH, 0);

        enterpriseOps = new EnterpriseOperations(this);
        enterpriseOps.open();
        switch (category){
            case 1:
                tittleArgTextView.setText("Consultancy");
                enterprises = enterpriseOps.getEnterpriseByCategory("category_1", "Consultancy");
                break;
            case 2:
                tittleArgTextView.setText("Custom development");
                enterprises = enterpriseOps.getEnterpriseByCategory("category_2","Custom development");
                break;
            case 3:
                tittleArgTextView.setText("Software developer");
                enterprises = enterpriseOps.getEnterpriseByCategory("category_3","Software developer");
                break;
        }

        enterpriseOps.close();
        ArrayAdapter<Enterprise> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, enterprises);
        setListAdapter(adapter);
    }
}
