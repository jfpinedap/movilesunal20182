package com.example.jfpinedap.enterprisedirectory.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EnterpriseDBHandler extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "enterprise.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_ENTERPRISE = "enterprise";
    public static final String COLUMN_ID = "entId";
    public static final String COLUMN_NAME = "entname";
    public static final String COLUMN_URL = "url_web";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PROSERV = "prodAndServ";
    public static final String COLUMN_CATEGORY_1= "category_1";
    public static final String COLUMN_CATEGORY_2= "category_2";
    public static final String COLUMN_CATEGORY_3= "category_3";

    private static final String TABLE_CREATE =
            "CREATE TABLE " + TABLE_ENTERPRISE + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_NAME + " TEXT, " +
                    COLUMN_URL + " TEXT, " +
                    COLUMN_PHONE + " TEXT, " +
                    COLUMN_EMAIL + " TEXT, " +
                    COLUMN_PROSERV + " TEXT, " +
                    COLUMN_CATEGORY_1 + " TEXT, " +
                    COLUMN_CATEGORY_2 + " TEXT, " +
                    COLUMN_CATEGORY_3 + " TEXT " +
                    ")";


    public EnterpriseDBHandler(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTERPRISE);
        db.execSQL(TABLE_CREATE);
    }
}
