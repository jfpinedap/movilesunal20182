package com.example.jfpinedap.enterprisedirectory;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jfpinedap.enterprisedirectory.DB.EnterpriseOperations;
import com.example.jfpinedap.enterprisedirectory.Model.Enterprise;

import java.util.List;

public class ViewNameEnterprise extends ListActivity {

    private static final String EXTRA_NAME_SEARCH = "com.example.jfpinedap.nameSearch";

    private EnterpriseOperations enterpriseOps;
    List<Enterprise> enterprises;

    TextView tittleTextView;
    TextView tittleArgTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_queries);

        tittleTextView = (TextView) findViewById(R.id.text_view_all_enterprises);
        tittleArgTextView = (TextView) findViewById(R.id.text_view_all_enterprises_arg);
        tittleTextView.setText(R.string.view_name_query);

        String name = getIntent().getStringExtra(EXTRA_NAME_SEARCH);

        tittleArgTextView.setText(name);

        enterpriseOps = new EnterpriseOperations(this);
        enterpriseOps.open();
        enterprises = enterpriseOps.getEnterpriseByName(name);
        enterpriseOps.close();
        ArrayAdapter<Enterprise> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, enterprises);
        setListAdapter(adapter);
    }
}
