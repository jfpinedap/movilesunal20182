package com.example.jfpinedap.enterprisedirectory;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.jfpinedap.enterprisedirectory.DB.EnterpriseOperations;

import java.util.ArrayList;
import java.util.List;

public class ManagementActivity extends AppCompatActivity {

    private EnterpriseOperations enterpriseOps;

    private Button addEnterpriseButton;
    private Button editEnterpriseButton;
    private Button deleteEnterpriseButton;
    private Button viewAllEnterpriseButton;
    private Button searchByNameButton;
    private Button searchByCategotyButton;

    private static final int DIALOG_EXIT_GAME_ID = 1;
    private static final int DIALOG_ABOUT_ID = 2;
    private static final int DIALOG_REMOVE_ALL_DB = 3;


    private static final String EXTRA_ENT_ID = "com.example.jfpinedap.entId";
    private static final String EXTRA_ADD_UPDATE = "com.example.jfpinedap.add_update";
    private static final String EXTRA_LAST_ID_ADDED = "com.example.jfpinedap.lastIdAdded";
    private static final String EXTRA_REMOVED_OK = "com.example.jfpinedap.removeOk";
    private static final String EXTRA_NAME_SEARCH = "com.example.jfpinedap.nameSearch";
    private static final String EXTRA_CATEGORY_SEARCH = "com.example.jfpinedap.category_search";
    private static final String EXTRA_REMOVE_ALL_DB = "com.example.jfpinedap.remove_all_db";


    private long lastIdAdded;

    private List<Long> removedIds = new ArrayList<>();

    private SharedPreferences mPrefs;

    private boolean removeIsOk = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management);

        enterpriseOps = new EnterpriseOperations(ManagementActivity.this);

        // persistence for IDs added and removed
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        removedIds.clear();
        int size = mPrefs.getInt("Status_size", 0);

        for(int i=0;i<size;i++)
        {
            removedIds.add(mPrefs.getLong("Status_" + i, 0));
        }

        lastIdAdded = mPrefs.getLong("lastIdAdded", lastIdAdded);

        // catch last ID added
        lastIdAdded = getIntent().getLongExtra(EXTRA_LAST_ID_ADDED, lastIdAdded);

        // catch if remove action has been successful
        removeIsOk = getIntent().getBooleanExtra(EXTRA_REMOVED_OK, true);

        if (!removeIsOk) {
            removedIds.remove(removedIds.size()-1);
            removeIsOk = true;
        }

        //
        addEnterpriseButton = (Button) findViewById(R.id.button_add_entreprise);
        editEnterpriseButton = (Button) findViewById(R.id.button_edit_enterprise);
        deleteEnterpriseButton = (Button) findViewById(R.id.button_delete_enterprise);
        viewAllEnterpriseButton = (Button)findViewById(R.id.button_view_enterprises);
        searchByNameButton = (Button) findViewById(R.id.button_search_by_name);
        searchByCategotyButton = (Button) findViewById(R.id.button_search_by_category);

        addEnterpriseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ManagementActivity.this,AddUpdateEnterprise.class);
                i.putExtra(EXTRA_ADD_UPDATE, "Add");
                startActivity(i);

            }
        });

        editEnterpriseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEntIdAndUpdateEnt();
            }
        });

        deleteEnterpriseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEntIdAndRemoveEnt();
            }
        });

        viewAllEnterpriseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ManagementActivity.this, ViewAllEnterprise.class);
                startActivity(i);

            }
        });

        searchByNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSearchByNameEnt();
            }
        });

        searchByCategotyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSearchByCategotyEnt();
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.enterprise_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.removeDB:
                showDialog(DIALOG_REMOVE_ALL_DB );
                return true;
            case R.id.about:
                showDialog(DIALOG_ABOUT_ID);
                return true;

            case R.id.menu_exit:
                showDialog(DIALOG_EXIT_GAME_ID);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        switch(id) {
            case DIALOG_REMOVE_ALL_DB:
                // Create the quit confirmation dialog
                builder.setMessage(R.string.exit_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Intent i = new Intent(ManagementActivity.this,RemoveEnterprise.class);
                                i.putExtra(EXTRA_REMOVE_ALL_DB, true);
                                startActivity(i);

                            }
                        })
                        .setNegativeButton(R.string.no, null);
                dialog = builder.create();
                break;

            case DIALOG_ABOUT_ID:
                Context context = getApplicationContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.about_dialog, null);
                builder.setView(layout);
                builder.setPositiveButton("OK", null);
                dialog = builder.create();
                break;

            case DIALOG_EXIT_GAME_ID:
                // Create the quit confirmation dialog
                builder.setMessage(R.string.exit_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ManagementActivity.this.finish();
                            }
                        })
                        .setNegativeButton(R.string.no, null);
                dialog = builder.create();
                break;
        }
        return dialog;
    }


    public void getEntIdAndUpdateEnt(){

        LayoutInflater li = LayoutInflater.from(this);
        View getEmpIdView = li.inflate(R.layout.dialog_get_ent_id, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set dialog_get_emp_id.xml to alertdialog builder
        alertDialogBuilder.setView(getEmpIdView);

        final EditText userInput = (EditText) getEmpIdView.findViewById(R.id.editTextDialogUserInputId);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // get user input and set it to result
                        // edit text
                        long id_;

                        try {
                            id_ = Long.parseLong(userInput.getText().toString());
                        }
                        catch (IllegalArgumentException e){
                            Toast.makeText(ManagementActivity.this,"Illegal ID: " + e.getMessage(),Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (id_ > lastIdAdded){
                            Toast.makeText(ManagementActivity.this,"Last ID added: " + lastIdAdded + "\n The ID: " + id_ +  ", Has not been assigned yet!",Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (id_ <= 0){
                            Toast.makeText(ManagementActivity.this,"Illegal ID. \n The ID must be positive and different to zero!",Toast.LENGTH_LONG).show();
                            return;
                        }

                        for(int i=0;i<removedIds.size();i++)
                        {
                            if (removedIds.get(i).equals(id_)){
                                Toast.makeText(ManagementActivity.this,"Illegal ID: " + id_ + "\n This ID was removed!",Toast.LENGTH_LONG).show();
                                return;
                            }
                        }

                        Intent i = new Intent(ManagementActivity.this,AddUpdateEnterprise.class);
                        i.putExtra(EXTRA_ADD_UPDATE, "Update");
                        i.putExtra(EXTRA_ENT_ID, id_);
                        startActivity(i);


                    }
                }).create()
                .show();
    }

    public void getEntIdAndRemoveEnt(){

        LayoutInflater li = LayoutInflater.from(this);
        View getEmpIdView = li.inflate(R.layout.dialog_get_ent_id, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set dialog_get_emp_id.xml to alertdialog builder
        alertDialogBuilder.setView(getEmpIdView);

        final EditText userInput = (EditText) getEmpIdView.findViewById(R.id.editTextDialogUserInputId);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // get user input and set it to result
                        // edit text
                        long id_;

                        try {
                             id_ = Long.parseLong(userInput.getText().toString());
                        }
                        catch (IllegalArgumentException e){
                            Toast.makeText(ManagementActivity.this,"Illegal ID: " + e.getMessage(),Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (id_ > lastIdAdded){
                            Toast.makeText(ManagementActivity.this,"Last ID added: " + lastIdAdded + "\n The ID: " + id_ +  ", Has not been assigned yet!",Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (id_ <= 0){
                            Toast.makeText(ManagementActivity.this,"Illegal ID. \n The ID must be positive and different to zero!",Toast.LENGTH_LONG).show();
                            return;
                        }

                        for(int i=0;i<removedIds.size();i++)
                        {
                            if (removedIds.get(i).equals(id_)){
                                Toast.makeText(ManagementActivity.this,"Illegal ID: " + id_ + "\n This ID was already removed!",Toast.LENGTH_LONG).show();
                                return;
                            }
                        }

                        Intent i = new Intent(ManagementActivity.this,RemoveEnterprise.class);
                        i.putExtra(EXTRA_ENT_ID, id_);
                        removedIds.add(id_);
                        startActivity(i);


                    }
                }).create()
                .show();

    }

    public void getSearchByNameEnt(){

        LayoutInflater li = LayoutInflater.from(this);
        View getEmpIdView = li.inflate(R.layout.dialog_get_ent_name, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set dialog_get_emp_id.xml to alertdialog builder
        alertDialogBuilder.setView(getEmpIdView);

        final EditText userInput = (EditText) getEmpIdView.findViewById(R.id.editTextDialogUserInputName);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // get user input and set it to result
                        // edit text
                        String name;

                        try {
                            name = userInput.getText().toString();
                        }
                        catch (IllegalArgumentException e){
                            Toast.makeText(ManagementActivity.this,"Illegal ID: " + e.getMessage(),Toast.LENGTH_LONG).show();
                            return;
                        }

                        Intent i = new Intent(ManagementActivity.this, ViewNameEnterprise.class);
                        i.putExtra(EXTRA_NAME_SEARCH, name);
                        startActivity(i);


                    }
                }).create()
                .show();
    }

    public void getSearchByCategotyEnt(){

        LayoutInflater li = LayoutInflater.from(this);
        View getEmpIdView = li.inflate(R.layout.dialog_get_ent_category, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set dialog_get_emp_id.xml to alertdialog builder
        alertDialogBuilder.setView(getEmpIdView);

        final RadioGroup userInput = (RadioGroup) getEmpIdView.findViewById(R.id.radio_button_category);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // get user input and set it to result
                        // edit text
                        int category;


                        switch (userInput.getCheckedRadioButtonId()){
                            case R.id.radio_consultancy:
                                category = 1;
                                break;
                            case R.id.radio_custom:
                                category = 2;
                                break;
                            case R.id.radio_software:
                                category = 3;
                                break;
                            default:
                                category = 0;
                        }


                        Intent i = new Intent(ManagementActivity.this, ViewCategoryEnterprise.class);
                        i.putExtra(EXTRA_CATEGORY_SEARCH, category);
                        startActivity(i);


                    }
                }).create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        enterpriseOps.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        enterpriseOps.close();
    }
    @Override
    protected void onStop() {
        super.onStop();

        // Save the current ID
        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putLong("lastIdAdded", lastIdAdded);

        /* removedIds is an array */
        ed.putInt("Status_size", removedIds.size());

        for(int i=0;i<removedIds.size();i++)
        {
            ed.remove("Status_" + i);
            ed.putLong("Status_" + i, removedIds.get(i));
        }

        ed.apply();
    }
}
