package com.example.jfpinedap.enterprisedirectory;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jfpinedap.enterprisedirectory.DB.EnterpriseOperations;
import com.example.jfpinedap.enterprisedirectory.Model.Enterprise;

public class AddUpdateEnterprise extends AppCompatActivity {

    private static final String EXTRA_ENT_ID = "com.example.jfpinedap.entId";
    private static final String EXTRA_ADD_UPDATE = "com.example.jfpinedap.add_update";
    private static final String EXTRA_LAST_ID_ADDED = "com.example.jfpinedap.lastIdAdded";
    private CheckBox consultancyCheckBox, customCheckBox, softwareCheckBox;
    private EditText nameEditText;
    private EditText urlEditText;
    private EditText phoneEditText;
    private EditText emailEditText;
    private EditText proservEditText;
    private Button addUpdateButton;
    private Enterprise newEnterprise;
    private Enterprise oldEnterprise;
    private String mode;
    private long entId;
    private long lastIdAdded;
    private EnterpriseOperations enterpriseData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_update_enterprise);

        newEnterprise = new Enterprise();
        oldEnterprise = new Enterprise();
        nameEditText = (EditText)findViewById(R.id.edit_text_ent_name);
        urlEditText = (EditText)findViewById(R.id.edit_text_url);
        phoneEditText = (EditText) findViewById(R.id.edit_text_phone);
        emailEditText = (EditText) findViewById(R.id.edit_text_email);
        proservEditText = (EditText) findViewById(R.id.edit_text_prod_serv);
        consultancyCheckBox = (CheckBox) findViewById(R.id.checkbox_consultancy);
        customCheckBox = (CheckBox) findViewById(R.id.checkbox_custom_development);
        softwareCheckBox = (CheckBox) findViewById(R.id.checkbox_software_developers);
        addUpdateButton = (Button)findViewById(R.id.button_add_update_enterprise);
        enterpriseData = new EnterpriseOperations(this);
        enterpriseData.open();


        mode = getIntent().getStringExtra(EXTRA_ADD_UPDATE);
        if(mode.equals("Update")){

            addUpdateButton.setText("Update Enterprise");
            entId = getIntent().getLongExtra(EXTRA_ENT_ID,0);

            initializeEnterprise(entId);

        }

        consultancyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mode.equals("Add")){
                    newEnterprise.setCategory_1(isChecked ? "Consultancy" : null);
                } else if (mode.equals("Update")){
                    oldEnterprise.setCategory_1(isChecked ? "Consultancy" : null);
                }
            }
        });

        customCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mode.equals("Add")){
                    newEnterprise.setCategory_2(isChecked ? "Custom development" : null);
                } else if (mode.equals("Update")){
                    oldEnterprise.setCategory_2(isChecked ? "Custom development" : null);
                }
            }
        });

        softwareCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mode.equals("Add")){
                    newEnterprise.setCategory_3(isChecked ? "Software developer" : null);
                } else if (mode.equals("Update")){
                    oldEnterprise.setCategory_3(isChecked ? "Software developer" : null);
                }
            }
        });


        addUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mode.equals("Add")) {
                    if (nameEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        nameEditText.setError("Please enter Company name");
                        return;
                    }
                    if (urlEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        urlEditText.setError("Please enter URL web page");
                        return;
                    }
                    if (phoneEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        phoneEditText.setError("Please enter Company phone");
                        return;
                    }
                    if (emailEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        emailEditText.setError("Please enter Company e-mail");
                        return;
                    }
                    if (proservEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        proservEditText.setError("Please enter Company Products and/or services");
                        return;
                    }
                    if (!consultancyCheckBox.isChecked() && !customCheckBox.isChecked() && !softwareCheckBox.isChecked()){
                        //it gives user to info message //use any one /
                        proservEditText.setError("Please select at least one Category");
                        Toast.makeText(AddUpdateEnterprise.this, "Please select at least one Category", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    newEnterprise.setEntname(nameEditText.getText().toString());
                    newEnterprise.setUrl_web(urlEditText.getText().toString());
                    newEnterprise.setPhone(phoneEditText.getText().toString());
                    newEnterprise.setEmail(emailEditText.getText().toString());
                    newEnterprise.setProdAndServ(proservEditText.getText().toString());
                    enterpriseData.addEnterprise(newEnterprise);
                    Toast.makeText(AddUpdateEnterprise.this, "Enterprise "+ newEnterprise.getEntname() + " has been added successfully !", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(AddUpdateEnterprise.this,ManagementActivity.class);
                    lastIdAdded = newEnterprise.getEntId();
                    i.putExtra(EXTRA_LAST_ID_ADDED, lastIdAdded);
                    startActivity(i);
                    finishAffinity();
                }else {
                    if (nameEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        nameEditText.setError("Please enter Company name");
                        return;
                    }
                    if (urlEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        urlEditText.setError("Please enter URL web page");
                        return;
                    }
                    if (phoneEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        phoneEditText.setError("Please enter Company phone");
                        return;
                    }
                    if (emailEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        emailEditText.setError("Please enter Company e-mail");
                        return;
                    }
                    if (proservEditText.getText().toString().equals("")){
                        //it gives user to info message //use any one /
                        proservEditText.setError("Please enter Company Products and/or services");
                        return;
                    }
                    if (!consultancyCheckBox.isChecked() && !customCheckBox.isChecked() && !softwareCheckBox.isChecked()){
                        //it gives user to info message //use any one /
                        proservEditText.setError("Please select at least one Category");
                        Toast.makeText(AddUpdateEnterprise.this, "Please select at least one Category", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    oldEnterprise.setEntname(nameEditText.getText().toString());
                    oldEnterprise.setUrl_web(urlEditText.getText().toString());
                    oldEnterprise.setEmail(emailEditText.getText().toString());
                    oldEnterprise.setPhone(phoneEditText.getText().toString());
                    oldEnterprise.setProdAndServ(proservEditText.getText().toString());
                    enterpriseData.updateEnterprise(oldEnterprise);
                    Toast t = Toast.makeText(AddUpdateEnterprise.this, "Enterprise "+ oldEnterprise.getEntname() + " has been updated successfully !", Toast.LENGTH_SHORT);
                    t.show();
                    Intent i = new Intent(AddUpdateEnterprise.this,ManagementActivity.class);
                    startActivity(i);
                    finishAffinity();

                }


            }
        });
    }

    private void initializeEnterprise(long entId) {

        oldEnterprise = enterpriseData.getEnterprise(entId);

        nameEditText.setText(oldEnterprise.getEntname());
        urlEditText.setText(oldEnterprise.getUrl_web());
        phoneEditText.setText(oldEnterprise.getPhone());
        emailEditText.setText(oldEnterprise.getEmail());
        proservEditText.setText(oldEnterprise.getProdAndServ());
        if (oldEnterprise.getCategory_1() != null) {
            if (oldEnterprise.getCategory_1().equals("Consultancy"))
                consultancyCheckBox.setChecked(true);
            else consultancyCheckBox.setChecked(false);
        }

        if (oldEnterprise.getCategory_2() != null) {
            if (oldEnterprise.getCategory_2().equals("Custom development"))
                customCheckBox.setChecked(true);
            else customCheckBox.setChecked(false);
        }

        if (oldEnterprise.getCategory_3() != null) {
            if (oldEnterprise.getCategory_3().equals("Software developer"))
                softwareCheckBox.setChecked(true);
            else softwareCheckBox.setChecked(false);
        }

    }
}
