package com.example.jfpinedap.enterprisedirectory;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.jfpinedap.enterprisedirectory.DB.EnterpriseOperations;
import com.example.jfpinedap.enterprisedirectory.Model.Enterprise;

import java.util.List;

public class ViewAllEnterprise extends ListActivity {

    private EnterpriseOperations enterpriseOps;
    List<Enterprise> enterprises;

    TextView tittleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_queries);

        tittleTextView = (TextView) findViewById(R.id.text_view_all_enterprises);
        tittleTextView.setText(R.string.view_all_enterprises_query);

        enterpriseOps = new EnterpriseOperations(this);
        enterpriseOps.open();
        enterprises = enterpriseOps.getAllEnterprises();
        enterpriseOps.close();
        ArrayAdapter<Enterprise> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, enterprises);
        setListAdapter(adapter);
    }
}
