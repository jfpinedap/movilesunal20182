package com.example.jfpinedap.enterprisedirectory.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.jfpinedap.enterprisedirectory.Model.Enterprise;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseOperations {

    public static final String LOGTAG = "EMP_MNGMNT_SYS";

    SQLiteOpenHelper dbhandler;
    SQLiteDatabase database;

    private static final String[] allColumns = {
            EnterpriseDBHandler.COLUMN_ID,
            EnterpriseDBHandler.COLUMN_NAME,
            EnterpriseDBHandler.COLUMN_URL,
            EnterpriseDBHandler.COLUMN_PHONE,
            EnterpriseDBHandler.COLUMN_EMAIL,
            EnterpriseDBHandler.COLUMN_PROSERV,
            EnterpriseDBHandler.COLUMN_CATEGORY_1,
            EnterpriseDBHandler.COLUMN_CATEGORY_2,
            EnterpriseDBHandler.COLUMN_CATEGORY_3

    };

    public EnterpriseOperations(Context context) {
        dbhandler = new EnterpriseDBHandler(context);
    }

    public void open() {
        Log.i(LOGTAG, "Database Opened");
        database = dbhandler.getWritableDatabase();
    }

    public void close() {
        Log.i(LOGTAG, "Database Closed");
        dbhandler.close();

    }

    public Enterprise addEnterprise(Enterprise Enterprise) {
        ContentValues values = new ContentValues();
        values.put(EnterpriseDBHandler.COLUMN_NAME, Enterprise.getEntname());
        values.put(EnterpriseDBHandler.COLUMN_URL, Enterprise.getUrl_web());
        values.put(EnterpriseDBHandler.COLUMN_PHONE, Enterprise.getPhone());
        values.put(EnterpriseDBHandler.COLUMN_EMAIL, Enterprise.getEmail());
        values.put(EnterpriseDBHandler.COLUMN_PROSERV, Enterprise.getProdAndServ());
        values.put(EnterpriseDBHandler.COLUMN_CATEGORY_1, Enterprise.getCategory_1());
        values.put(EnterpriseDBHandler.COLUMN_CATEGORY_2, Enterprise.getCategory_2());
        values.put(EnterpriseDBHandler.COLUMN_CATEGORY_3, Enterprise.getCategory_3());
        long insertid = database.insert(EnterpriseDBHandler.TABLE_ENTERPRISE, null, values);
        Enterprise.setEntId(insertid);
        return Enterprise;

    }

    // Getting single Enterprise
    public Enterprise getEnterprise(long id) {

        Cursor cursor = database.query(EnterpriseDBHandler.TABLE_ENTERPRISE, allColumns, EnterpriseDBHandler.COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();

            Enterprise e = new Enterprise(Long.parseLong(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2),
                    cursor.getString(3), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6),
                    cursor.getString(7), cursor.getString(8));
            // return Employee
            return e;
        }
        return null;
    }

    public List<Enterprise> getAllEnterprises() {

        Cursor cursor = database.query(EnterpriseDBHandler.TABLE_ENTERPRISE, allColumns, null, null, null, null, null);

        List<Enterprise> enterprises = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Enterprise enterprise = new Enterprise();
                enterprise.setEntId(cursor.getLong(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_ID)));
                enterprise.setEntname(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_NAME)));
                enterprise.setUrl_web(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_URL)));
                enterprise.setPhone(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PHONE)));
                enterprise.setEmail(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_EMAIL)));
                enterprise.setProdAndServ(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PROSERV)));
                enterprise.setCategory_1(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_1)));
                enterprise.setCategory_2(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_2)));
                enterprise.setCategory_3(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_3)));
                enterprises.add(enterprise);
            }
        }
        // return All Employees
        return enterprises;
    }


    public List<Enterprise> getEnterpriseById(long id) {

        Cursor cursor = database.query(EnterpriseDBHandler.TABLE_ENTERPRISE, allColumns, EnterpriseDBHandler.COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        List<Enterprise> enterprises = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Enterprise enterprise = new Enterprise();
                enterprise.setEntId(cursor.getLong(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_ID)));
                enterprise.setEntname(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_NAME)));
                enterprise.setUrl_web(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_URL)));
                enterprise.setPhone(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PHONE)));
                enterprise.setEmail(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_EMAIL)));
                enterprise.setProdAndServ(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PROSERV)));
                enterprise.setCategory_1(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_1)));
                enterprise.setCategory_2(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_2)));
                enterprise.setCategory_3(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_3)));
                enterprises.add(enterprise);
            }
        }
        // return All Employees
        return enterprises;
    }


    public List<Enterprise> getEnterpriseByName(String name) {

        Cursor cursor = database.query(EnterpriseDBHandler.TABLE_ENTERPRISE, allColumns, EnterpriseDBHandler.COLUMN_NAME + "=?", new String[]{name}, null, null, null, null);

        List<Enterprise> enterprises = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Enterprise enterprise = new Enterprise();
                enterprise.setEntId(cursor.getLong(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_ID)));
                enterprise.setEntname(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_NAME)));
                enterprise.setUrl_web(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_URL)));
                enterprise.setPhone(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PHONE)));
                enterprise.setEmail(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_EMAIL)));
                enterprise.setProdAndServ(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PROSERV)));
                enterprise.setCategory_1(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_1)));
                enterprise.setCategory_2(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_2)));
                enterprise.setCategory_3(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_3)));
                enterprises.add(enterprise);
            }
        }
        // return All Employees
        return enterprises;
    }


    public List<Enterprise> getEnterpriseByCategory(String category_column, String category) {
        Cursor cursor = database.query(EnterpriseDBHandler.TABLE_ENTERPRISE, allColumns, category_column + "=?", new String[]{category}, null, null, null, null);

        List<Enterprise> enterprises = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Enterprise enterprise = new Enterprise();
                enterprise.setEntId(cursor.getLong(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_ID)));
                enterprise.setEntname(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_NAME)));
                enterprise.setUrl_web(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_URL)));
                enterprise.setPhone(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PHONE)));
                enterprise.setEmail(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_EMAIL)));
                enterprise.setProdAndServ(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_PROSERV)));
                enterprise.setCategory_1(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_1)));
                enterprise.setCategory_2(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_2)));
                enterprise.setCategory_3(cursor.getString(cursor.getColumnIndex(EnterpriseDBHandler.COLUMN_CATEGORY_3)));
                enterprises.add(enterprise);
            }
        }
        // return All Employees
        return enterprises;
    }

    // Updating Enterprise
    public int updateEnterprise(Enterprise enterprise) {

        ContentValues values = new ContentValues();
        values.put(EnterpriseDBHandler.COLUMN_NAME, enterprise.getEntname());
        values.put(EnterpriseDBHandler.COLUMN_URL, enterprise.getUrl_web());
        values.put(EnterpriseDBHandler.COLUMN_PHONE, enterprise.getPhone());
        values.put(EnterpriseDBHandler.COLUMN_EMAIL, enterprise.getEmail());
        values.put(EnterpriseDBHandler.COLUMN_PROSERV, enterprise.getProdAndServ());
        values.put(EnterpriseDBHandler.COLUMN_CATEGORY_1, enterprise.getCategory_1());
        values.put(EnterpriseDBHandler.COLUMN_CATEGORY_2, enterprise.getCategory_2());
        values.put(EnterpriseDBHandler.COLUMN_CATEGORY_3, enterprise.getCategory_3());

        // updating row
        return database.update(EnterpriseDBHandler.TABLE_ENTERPRISE, values,
                EnterpriseDBHandler.COLUMN_ID + "=?", new String[]{String.valueOf(enterprise.getEntId())});
    }

    // Deleting Enterprise
    public int removeEnterprise(Enterprise enterprise) {

        int idRemoved = database.delete(EnterpriseDBHandler.TABLE_ENTERPRISE, EnterpriseDBHandler.COLUMN_ID + "=" + enterprise.getEntId(), null);
        return idRemoved;
    }

    // Deleting Enterprise
    public int removeAll() {

        database = dbhandler.getWritableDatabase();
        int deletedRows = database.delete(EnterpriseDBHandler.TABLE_ENTERPRISE, "1", null);
        return deletedRows;
    }
}