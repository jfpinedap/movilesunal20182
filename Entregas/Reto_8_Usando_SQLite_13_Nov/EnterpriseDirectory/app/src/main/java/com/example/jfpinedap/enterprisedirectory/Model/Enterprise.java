package com.example.jfpinedap.enterprisedirectory.Model;

import android.support.annotation.NonNull;

public class Enterprise {
    private long entId;
    private String entname;
    private String url_web;
    private String phone;
    private String email;
    private String prodAndServ;
    private String category_1;
    private String category_2;
    private String category_3;

    public Enterprise(long entId, String entname, String url_web, String phone, String email, String prodAndServ, String category_1, String category_2, String category_3){
        this.entId = entId;
        this.entname = entname;
        this.url_web = url_web;
        this.phone = phone;
        this.email = email;
        this.prodAndServ = prodAndServ;
        this.category_1 = category_1;
        this.category_2 = category_2;
        this.category_3 = category_3;
    }

    public Enterprise(){

    }

    public long getEntId() {
        return entId;
    }

    public void setEntId(long entId) {
        this.entId = entId;
    }

    public String getEntname() {
        return entname;
    }

    public void setEntname(String entname) {
        this.entname = entname;
    }

    public String getUrl_web() {
        return url_web;
    }

    public void setUrl_web(String url_web) {
        this.url_web = url_web;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProdAndServ() {
        return prodAndServ;
    }

    public void setProdAndServ(String prodAndServ) {
        this.prodAndServ = prodAndServ;
    }

    public String getCategory_1() {
        return category_1;
    }

    public void setCategory_1(String category_1) {
        this.category_1 = category_1;
    }

    public String getCategory_2() {
        return category_2;
    }

    public void setCategory_2(String category_2) {
        this.category_2 = category_2;
    }

    public String getCategory_3() {
        return category_3;
    }

    public void setCategory_3(String category_3) {
        this.category_3 = category_3;
    }

    public String getCategory() {
        StringBuilder cat = new StringBuilder();
        if (getCategory_1() != null){
            cat.append("\n       ").append(getCategory_1());
        }
        if (getCategory_2() != null){
            cat.append("\n       ").append(getCategory_2());
        }
        if (getCategory_3() != null){
            cat.append("\n       ").append(getCategory_3());
        }
        return cat.toString();
    }



    @NonNull
    @Override
    public String toString() {
        return "Enterpise id: " + getEntId() + "\n" +
                "Name: " + getEntname() + "\n" +
                "URL web page: " + getUrl_web() + "\n" +
                "Phone: " + getPhone() + "\n" +
                "e-mail: "+ getEmail() + "\n" +
                "Products & services: " + getProdAndServ() + "\n" +
                "Categoty: " + getCategory();
    }
}
