package com.example.jfpinedap.enterprisedirectory;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jfpinedap.enterprisedirectory.DB.EnterpriseOperations;
import com.example.jfpinedap.enterprisedirectory.Model.Enterprise;


public class RemoveEnterprise extends AppCompatActivity {

    static final int REMOVE_ID = 1;

    private static final String EXTRA_ENT_ID = "com.example.jfpinedap.entId";
    private static final String EXTRA_REMOVED_OK = "com.example.jfpinedap.removeOk";
    private static final String EXTRA_REMOVE_ALL_DB = "com.example.jfpinedap.remove_all_db";

    private TextView nameTextView;
    private TextView urlTextView;
    private TextView phoneTextView;
    private TextView emailTextView;
    private TextView proservTextView;
    private TextView categoryTextView;
    private Enterprise enterprise;
    private Button removeButton;
    private long entId;
    private boolean removeAll;
    private EnterpriseOperations enterpriseData;
    private boolean currentStatus = false;

    private SharedPreferences mPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_enterprise);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        currentStatus = mPrefs.getBoolean("currentStatus", currentStatus);

        enterprise = new Enterprise();
        nameTextView = (TextView) findViewById(R.id.text_view_ent_name);
        urlTextView = (TextView) findViewById(R.id.text_view_url);
        phoneTextView = (TextView) findViewById(R.id.text_view_phone);
        emailTextView = (TextView) findViewById(R.id.text_view_email);
        proservTextView = (TextView) findViewById(R.id.text_view_prod_serv);
        categoryTextView = (TextView)findViewById(R.id.text_view_category_text);
        removeButton = (Button)findViewById(R.id.button_remove_enterprise);
        enterpriseData = new EnterpriseOperations(this);
        enterpriseData.open();

        entId = getIntent().getLongExtra(EXTRA_ENT_ID,0);

        removeAll = getIntent().getBooleanExtra(EXTRA_REMOVE_ALL_DB,false);

        if (removeAll) {
            int deletedRows = enterpriseData.removeAll();

            if(deletedRows > 0)
                Toast.makeText(RemoveEnterprise.this,deletedRows + " Records deleted",Toast.LENGTH_LONG).show();
            else
                Toast.makeText(RemoveEnterprise.this,"Data not Deleted",Toast.LENGTH_LONG).show();

            Intent i = new Intent(RemoveEnterprise.this,ManagementActivity.class);
            startActivity(i);
            finishAffinity();

        }else {

            initializeEnterprise(entId);
        }

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(REMOVE_ID);

            }
        });
    }

    private void initializeEnterprise(long entId) {


        enterprise = enterpriseData.getEnterprise(entId);


        nameTextView.setText(enterprise.getEntname());
        urlTextView.setText(enterprise.getUrl_web());
        phoneTextView.setText(enterprise.getPhone());
        emailTextView.setText(enterprise.getEmail());
        proservTextView.setText(enterprise.getProdAndServ());
        categoryTextView.setText(enterprise.getCategory());

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        switch(id) {
            case REMOVE_ID:
                // Create the quit confirmation dialog
                builder.setMessage(R.string.remove_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                int idRemoved = enterpriseData.removeEnterprise(enterprise);
                                if (idRemoved > 0){
                                    Toast.makeText(RemoveEnterprise.this, "Enterprise "+ enterprise.getEntname() + " has been removed successfully !", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(RemoveEnterprise.this,ManagementActivity.class);
                                    i.putExtra(EXTRA_REMOVED_OK, true);
                                    currentStatus = true;
                                    startActivity(i);
                                    finishAffinity();
                                }
                                else if (idRemoved == 0){
                                    Toast.makeText(RemoveEnterprise.this, "Enterprise "+ enterprise.getEntname() + " hasn't been removed successfully !", Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(RemoveEnterprise.this,ManagementActivity.class);
                                    i.putExtra(EXTRA_REMOVED_OK, false);
                                    startActivity(i);
                                    finishAffinity();
                                }
                            }
                        })
                        .setNegativeButton(R.string.no, null);
                dialog = builder.create();
                break;
        }
        return dialog;
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!currentStatus) {
            Intent i = new Intent(RemoveEnterprise.this, ManagementActivity.class);
            i.putExtra(EXTRA_REMOVED_OK, false);
            startActivity(i);
            finishAffinity();
            currentStatus = false;
        }

        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putBoolean("currentStatus", currentStatus);
        ed.apply();

    }

}
