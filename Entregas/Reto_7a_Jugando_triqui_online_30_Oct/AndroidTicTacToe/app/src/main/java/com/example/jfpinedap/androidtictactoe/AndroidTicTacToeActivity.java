package com.example.jfpinedap.androidtictactoe;



import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuInflater;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.widget.Toast;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.MessageFormat;
import java.util.Objects;

import static java.lang.Integer.valueOf;


public class AndroidTicTacToeActivity extends AppCompatActivity {

    // Constants which will be used to identify the two dialog boxes
//    static final int DIALOG_DIFFICULTY_ID = 0;
    static final int DIALOG_EXIT_GAME_ID = 1;
    static final int DIALOG_ABOUT_ID = 2;
    public static final int RC_SIGN_IN = 1;
    public static final String ANONYMOUS = "anonymous";
    public static String PACKAGE_NAME;

    // Firebase instance variables
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private String mUsername;

    private ListView mMessageListView;

    // Represents the internal state of the game
    private TicTacToeGame mGame;

    private SharedPreferences mPrefs;

    // Variable to define if play sound or not
    private boolean mSoundOn;

    // Various text displayed
    private TextView mInfoTextView;
    private TextView mPlayer1ScoreTextView;
    private TextView mPlayer2ScoreTextView;
    private TextView mPlayer1TagTextView;
    private TextView mPlayer2TagTextView;
    private TextView mTieScoreTextView;

    // Various game score records
    private int player1Score;
    private int player2Score;
    private int tieScore;

    // Who play at first? player or computer
    private char mTurn = TicTacToeGame.PLAYER_1;

    // Locks the board game
    private boolean mGameOver;

    private BoardView mBoardView;


    // MediaPlayer variables for the sound effects
    MediaPlayer mPlayer1MediaPlayer;
    MediaPlayer mPlayer2MediaPlayer;

    private char myPlayer;
    private String gameUid;
    private String opponentUid;
    private String opponentName;
    private String myName;

    private ValueEventListener mValueEventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_tic_tac_toe);

        PACKAGE_NAME = getApplicationContext().getPackageName();

        // Initialize firebase
        mUsername = ANONYMOUS;
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mInfoTextView = (TextView) findViewById(R.id.info_label);
        mPlayer1ScoreTextView = (TextView) findViewById(R.id.player1_score);
        mPlayer2ScoreTextView = (TextView) findViewById(R.id.player2_score);
        mPlayer1TagTextView = (TextView) findViewById(R.id.player1_score_tag);
        mPlayer2TagTextView = (TextView) findViewById(R.id.player2_score_tag);
        mTieScoreTextView = (TextView) findViewById(R.id.tie_score);

        // Initialize references to views

        mMessageListView = (ListView) findViewById(R.id.messageListView);

        mGame = new TicTacToeGame();
        mBoardView = (BoardView) findViewById(R.id.board);
        mBoardView.setGame(mGame);

        // Listen for touches on the board
        mBoardView.setOnTouchListener(mTouchListener);


        Intent selectIntent = getIntent();

        //The second parameter below is the default string returned if the value is not there.
        gameUid = Objects.requireNonNull(selectIntent.getExtras()).getString("player1Uid","");
        opponentUid = Objects.requireNonNull(selectIntent.getExtras()).getString("player2Uid","");

        String name = selectIntent.getExtras().getString("opponentName","");

        myName = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getDisplayName();
        opponentName = name;


        if (Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid().equals(gameUid)){
            myPlayer = TicTacToeGame.PLAYER_1;
        }else {
            myPlayer = TicTacToeGame.PLAYER_2;
        }

        mDatabase.child("Game").child(gameUid).child("turn").setValue(String.valueOf(TicTacToeGame.PLAYER_1));

        myName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();

        // Restore the scores from the persistent preference data source
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Restore sound settings
        mSoundOn = mPrefs.getBoolean("sound", true);

        // Restore board color settings
        mBoardView.setBoardColor(mPrefs.getInt("board_color", Color.LTGRAY));

        // Restore the scores
//        player1Score = Integer.parseInt(mDatabase.child("player_1_Score").getKey());
//        player2Score = Integer.parseInt(mDatabase.child("player_2_Score").getKey());
//        tieScore = Integer.parseInt(mDatabase.child("tie_Score").getKey());


//        // Restore the scores
        player1Score = mPrefs.getInt("mHumanWins", 0);
        player2Score = mPrefs.getInt("mComputerWins", 0);
        tieScore = mPrefs.getInt("mTies", 0);

        // Restore the difficulty level saved on mPrefs
        String difficultyLevel = mPrefs.getString("difficulty_level",
                getResources().getString(R.string.difficulty_harder));
        if (Objects.equals(difficultyLevel, getResources().getString(R.string.difficulty_easy)))
            mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Easy);
        else if (Objects.equals(difficultyLevel, getResources().getString(R.string.difficulty_harder)))
            mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Harder);
        else
            mGame.setDifficultyLevel(TicTacToeGame.DifficultyLevel.Expert);
//        mGame.setDifficultyLevel((TicTacToeGame.DifficultyLevel.values()[mPrefs.getInt("difficultyLevel",
//                TicTacToeGame.DifficultyLevel.Easy.ordinal())]));


        if (savedInstanceState == null) {
            startNewGame();
        }


        mValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot gamesSnapshot = dataSnapshot.child("Game");
                Iterable<DataSnapshot> gamesChildren = gamesSnapshot.getChildren();

                for (DataSnapshot games : gamesChildren) {
                    DataGame game = games.getValue(DataGame.class);
                    //Toast.makeText(getApplicationContext(), "getBoard  " + game.getBoard() +  "  :  " + game.getPlayer_1() + "  :  " + gameUid, Toast.LENGTH_SHORT).show();
                    if (Objects.equals(Objects.requireNonNull(game).getPlayer_1(), gameUid)
                            && game.getBoard() != null) {

                        if (mGame.setMove(game.getTurn(), Integer.parseInt(Objects.requireNonNull(game).getBoard()))) {

                            mBoardView.invalidate();

                            if (mSoundOn && mTurn == TicTacToeGame.PLAYER_1) {
                                mPlayer2MediaPlayer.start();
                            } else {
                                mPlayer1MediaPlayer.start();
                            }

                            if (mTurn == TicTacToeGame.PLAYER_1) {
                                mDatabase.child("Game").child(gameUid).child("turn").setValue(String.valueOf(TicTacToeGame.PLAYER_2));
                                mTurn = TicTacToeGame.PLAYER_2;
                            } else {
                                mDatabase.child("Game").child(gameUid).child("turn").setValue(String.valueOf(TicTacToeGame.PLAYER_1));
                                mTurn = TicTacToeGame.PLAYER_1;
                            }

                            if (mTurn == myPlayer) {
                                mInfoTextView.setText(R.string.my_turn);
                            } else {
                                mInfoTextView.setText(R.string.opponent_turn);
                            }

                            // If no winner yet, let the computer make a move
                            int winner = mGame.checkForWinner();
                            if (winner != 0) {
                                publishResultGame(winner);
                            }
                        }
                    }

                    if (Objects.equals(Objects.requireNonNull(game).getPlayer_1(), gameUid)
                            && game.getNewGame() != null) {

                        if (game.getNewGame().equals("newGame")) {
                            startNewGame();
                            mDatabase.child("Game").child(gameUid).child("newGame").setValue(null);
                        }

                        if (game.getNewGame().equals("exit")) {

                            mDatabase.child("Game").setValue(null);

                            Intent gameIntent = new Intent(AndroidTicTacToeActivity.this, SelectGamers.class);
//                            gameIntent.putExtra("player1Score", player1Score);
//                            gameIntent.putExtra("player2Score", player2Score);
//                            gameIntent.putExtra("opponentUid", opponentUid);
                            player1Score = 0;
                            player2Score = 0;
                            tieScore = 0;
                            AndroidTicTacToeActivity.this.startActivity(gameIntent);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //handle databaseError
            }
        };
        mDatabase.addValueEventListener(mValueEventListener);
    }



    private void attachDatabaseReadListener() {

        if (mValueEventListener == null) {
            mValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    DataSnapshot gamesSnapshot = dataSnapshot.child("Game");
                    Iterable<DataSnapshot> gamesChildren = gamesSnapshot.getChildren();

                    for (DataSnapshot games : gamesChildren) {
                        DataGame game = games.getValue(DataGame.class);
                        //Toast.makeText(getApplicationContext(), "getBoard  " + game.getBoard() +  "  :  " + game.getPlayer_1() + "  :  " + gameUid, Toast.LENGTH_SHORT).show();
                        if (Objects.equals(Objects.requireNonNull(game).getPlayer_1(), gameUid)
                                && game.getBoard() != null) {

                            if (mGame.setMove(game.getTurn(), Integer.parseInt(Objects.requireNonNull(game).getBoard()))) {

                                mBoardView.invalidate();

                                if (mSoundOn && mTurn == TicTacToeGame.PLAYER_1) {
                                    mPlayer2MediaPlayer.start();
                                } else {
                                    mPlayer1MediaPlayer.start();
                                }

                                if (mTurn == TicTacToeGame.PLAYER_1) {
                                    mDatabase.child("Game").child(gameUid).child("turn").setValue(String.valueOf(TicTacToeGame.PLAYER_2));
                                    mTurn = TicTacToeGame.PLAYER_2;
                                } else {
                                    mDatabase.child("Game").child(gameUid).child("turn").setValue(String.valueOf(TicTacToeGame.PLAYER_1));
                                    mTurn = TicTacToeGame.PLAYER_1;
                                }

                                if (mTurn == myPlayer) {
                                    mInfoTextView.setText(R.string.my_turn);
                                } else {
                                    mInfoTextView.setText(R.string.opponent_turn);
                                }

                                // If no winner yet, let the computer make a move
                                int winner = mGame.checkForWinner();
                                if (winner != 0) {
                                    publishResultGame(winner);
                                }
                            }
                        }

                        if (Objects.equals(Objects.requireNonNull(game).getPlayer_1(), gameUid)
                                && game.getNewGame() != null) {

                            if (game.getNewGame().equals("newGame")) {
                                startNewGame();
                                mDatabase.child("Game").child(gameUid).child("newGame").setValue(null);
                            }

                            if (game.getNewGame().equals("exit")) {

                                mDatabase.child("Game").setValue(null);

                                Intent gameIntent = new Intent(AndroidTicTacToeActivity.this, SelectGamers.class);
//                                gameIntent.putExtra("player1Score", player1Score);
//                                gameIntent.putExtra("player2Score", player2Score);
//                                gameIntent.putExtra("opponentUid", opponentUid);
                                player1Score = 0;
                                player2Score = 0;
                                tieScore = 0;
                                AndroidTicTacToeActivity.this.startActivity(gameIntent);
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //handle databaseError
                }
            };
            mDatabase.addValueEventListener(mValueEventListener);
        }
    }

    private void detachDatabaseReadListener() {
        if (mValueEventListener != null) {
            mDatabase.removeEventListener(mValueEventListener);
            mValueEventListener = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_t, menu);
        return true;
/*        super.onCreateOptionsMenu(menu);
        menu.add("New Game");
        menu.add("Restore Scores");
        return true;*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
//            case R.id.difficulty:
//                showDialog(DIALOG_DIFFICULTY_ID);
//                return true;
            case R.id.settings_menu:
                startActivityForResult(new Intent(this, Settings.class), 0);
                return true;
            case R.id.about_menu:
                showDialog(DIALOG_ABOUT_ID);
                return true;
            case R.id.exit_game_menu:
                mDatabase.child("Users").child(gameUid).child("userScore").setValue(player1Score);
                mDatabase.child("Users").child(opponentUid).child("userScore").setValue(player2Score);
                mDatabase.child("Game").child(gameUid).child("newGame").setValue("exit");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        switch(id) {
            case DIALOG_EXIT_GAME_ID:
                // Create the quit confirmation dialog
                builder.setMessage(R.string.exit_game_question)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AndroidTicTacToeActivity.this.finish();
                            }
                        })
                        .setNegativeButton(R.string.no, null);
                dialog = builder.create();
                break;

            case DIALOG_ABOUT_ID:
                Context context = getApplicationContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.about_dialog, null);
                builder.setView(layout);
                builder.setPositiveButton("OK", null);
                dialog = builder.create();
                break;
        }
        return dialog;
    }

    @Override
    protected void onResume() {
        super.onResume();
        attachDatabaseReadListener();
        mPlayer1MediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sword);
        mPlayer2MediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.swish);
    }

    @Override
    protected void onPause() {
        super.onPause();
        detachDatabaseReadListener();
        mPlayer1MediaPlayer.release();
        mPlayer2MediaPlayer.release();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharArray("board", mGame.getBoardState());
        outState.putBoolean("mGameOver", mGameOver);
        outState.putCharSequence("mInfoTextView", mInfoTextView.getText());
        outState.putInt("player1Score", player1Score);
        outState.putInt("player2Score", player2Score);
        outState.putInt("tieScore", tieScore);
        outState.putChar("mTurn", mTurn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mGame.setBoardState(savedInstanceState.getCharArray("board"));
        mGameOver = savedInstanceState.getBoolean("mGameOver");
        mInfoTextView.setText(savedInstanceState.getCharSequence("mInfoTextView"));
        player1Score = savedInstanceState.getInt("player1Score");
        player2Score = savedInstanceState.getInt("player2Score");
        tieScore = savedInstanceState.getInt("tieScore");
        mTurn = savedInstanceState.getChar("mTurn");
        displayScores();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Save the current scores
        SharedPreferences.Editor ed =   mPrefs.edit();
        ed.putInt("mHumanWins", player1Score);
        ed.putInt("mComputerWins", player2Score);
        ed.putInt("mTies", tieScore);
        ed.putInt("difficultyLevel", mGame.getDifficultyLevel().ordinal());
        ed.apply();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RESULT_CANCELED) {
            // Apply potentially new settings

            mSoundOn = mPrefs.getBoolean("sound", true);


            mBoardView.setBoardColor(mPrefs.getInt(Settings.BOARD_COLOR, Color.LTGRAY));
        }

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, R.string.sing_in_short_lable, Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, R.string.sing_in_canceled_lable, Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }


    private void displayScores() {

        mTieScoreTextView.setText(MessageFormat.format("{0} ", Integer.toString(tieScore)));

        if(mTurn != myPlayer){
            mPlayer1ScoreTextView.setText(MessageFormat.format("{0} ", Integer.toString(player1Score)));
            mPlayer2ScoreTextView.setText(MessageFormat.format("{0} ",  Integer.toString(player2Score)));
            mPlayer1TagTextView.setText(MessageFormat.format("{0}  ",  myName));
            mPlayer2TagTextView.setText(MessageFormat.format("{0}  ",  opponentName));
        }
        else{
            mPlayer1ScoreTextView.setText(MessageFormat.format("{0} ", Integer.toString(player2Score)));
            mPlayer2ScoreTextView.setText(MessageFormat.format("{0} ",  Integer.toString(player1Score)));
            mPlayer1TagTextView.setText(MessageFormat.format("{0}  ",  myName));
            mPlayer2TagTextView.setText(MessageFormat.format("{0}  ",  opponentName));
        }


    }

    // Set up the game board.
    private void startNewGame() {
        mGame.clearBoard();

        mBoardView.invalidate(); // Redraw the board

        if(mTurn == myPlayer){
            mInfoTextView.setText(R.string.my_turn);
        }
        else{
            mInfoTextView.setText(R.string.opponent_turn);
        }

        mGameOver = false;

        displayScores();

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentFirebaseUser != null){
            Toast.makeText(getApplicationContext(), "New Game",
                    Toast.LENGTH_SHORT).show();
        }
    }// End of startNewGame

    // Listen for touches on the board
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {

            // Determine which cell was touched
            int col = (int) event.getX() / mBoardView.getBoardCellWidth();
            int row = (int) event.getY() / mBoardView.getBoardCellHeight();
            int pos = row * 3 + col;
            if (!mGameOver && mTurn == myPlayer) {

                    mDatabase.child("Game").child(gameUid).child("board").setValue(String.valueOf(pos));

                }
                // So we aren't notified of continued events when finger is moved
                return false;
        }
    };

    private void publishResultGame(int winner){

        if (winner == 1) {
            tieScore++;
            mDatabase.child("Game").child(gameUid).child("tie_Score").setValue(String.valueOf(tieScore));

        } else if (winner == 2) {
                player1Score++;
                mDatabase.child("Game").child(gameUid).child("player_1_Score").setValue(String.valueOf(player1Score));

        } else if (winner == 3){
                player2Score++;
                mDatabase.child("Game").child(gameUid).child("player_2_Score").setValue(String.valueOf(player2Score));
        }

        mGameOver = true;
        mDatabase.child("Game").child(gameUid).child("board").setValue(null);
        mDatabase.child("Game").child(gameUid).child("newGame").setValue("newGame");
    }

}
