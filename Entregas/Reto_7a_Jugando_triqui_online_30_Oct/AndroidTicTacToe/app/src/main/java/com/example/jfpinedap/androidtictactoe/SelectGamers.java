package com.example.jfpinedap.androidtictactoe;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SelectGamers extends AppCompatActivity {
    // Constants
    public static final int RC_SIGN_IN = 1;
    public static final String ANONYMOUS = "anonymous";
    public static String PACKAGE_NAME;
    public static final String TAG = "NewPostActivity";

    private UserGame mUserGame;

    // Firebase instance variables
    private DatabaseReference mDatabase;
    private DatabaseReference mGameDatabase;
    private FirebaseDatabase mFirebaseDatabase;
    private ChildEventListener mChildEventListener;
    private ValueEventListener mValueEventListener;
    private ValueEventListener gValueEventListener;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private String mUsername;

    private ListView mMessageListView;
    private MessageAdapter mMessageAdapter;
    private ProgressBar mProgressBar;
    private int countG;
    private int countUsers;

    // Text display
    private TextView mInfoTextView;

    private SharedPreferences mPrefs;

    private int userScore;
    private String mPlayer2Uid = null;
    private String sPlayer2Uid;
    private String mPlayer2Name = null;
    private int player1Score;
    private int player2Score;
    private  boolean runGame;
    private String opponentUid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PACKAGE_NAME = getApplicationContext().getPackageName();

        // Initialize firebase
        mUsername = ANONYMOUS;
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
//        mUid = FirebaseAuth.getInstance().getCurrentUser().getMyUid();

        // [START initialize_database_ref]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mGameDatabase = FirebaseDatabase.getInstance().getReference().child("Game");

        // Initialize references to views
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mMessageListView = (ListView) findViewById(R.id.messageListView);
        mInfoTextView = (TextView) findViewById(R.id.choose);

        mInfoTextView.setText(R.string.choose_info_1);

        // Restore the scores from the persistent preference data source
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Restore isGaming and  userScore
        userScore = mPrefs.getInt("userScore", 0);

//            // Initialize message ListView and its adapter
        List<UserGame> users = new ArrayList<>();
        mMessageAdapter = new MessageAdapter(this, R.layout.item_message, users);
        mMessageListView.setAdapter(mMessageAdapter);

        Intent gameIntent = getIntent();

//        //The second parameter below is the default string returned if the value is not there.
//        int player1Score = gameIntent.getIntExtra("player1Score", 0);
//        int player2Score = gameIntent.getIntExtra("player2Score", 0);
//        Bundle bundle = getIntent().getExtras();
//        opponentUid = Objects.requireNonNull(bundle).getString("opponentUid");
//
//
//        mDatabase.child("Users").child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child("userScore").setValue(player1Score);
//        mDatabase.child("Users").child(opponentUid).child("userScore").setValue(player2Score);

        mMessageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Object item = ((ListView) parent).getAdapter().getItem(position);
//                    Toast.makeText(getBaseContext(), "countUsers" + String.valueOf(countUsers), Toast.LENGTH_LONG).show();

                mPlayer2Uid = null;
                mPlayer2Name = null;

                for(int i=0; i < countUsers; i++ )
                {
                    if (item.equals(mMessageListView.getAdapter().getItem(i)) && countUsers > 1) {

//                        Toast.makeText(getApplicationContext(), "innn" + String.valueOf(i), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "countUsers" + String.valueOf(countUsers) + Objects.requireNonNull(mMessageAdapter.getItem(i)).getName() + " " + String.valueOf(i), Toast.LENGTH_SHORT).show();

                        if (Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid().equals(Objects.requireNonNull(mMessageAdapter.getItem(i)).getMyUid())) {
                            mInfoTextView.setText(R.string.choose_info_2);
                        }
                        else if (!Objects.requireNonNull(mMessageAdapter.getItem(i)).getStatus().equals(UserGame.AVILABLE)) {
                            Toast.makeText(getApplicationContext(), R.string.choose_info_3 , Toast.LENGTH_SHORT).show();
                        }
                        else{

                            //Toast.makeText(getApplicationContext(), mDatabase.child("Users").child("isGaming").getKey(), Toast.LENGTH_SHORT).show();

                            mInfoTextView.setText(R.string.choose_info_1);

                            mPlayer2Uid = Objects.requireNonNull(mMessageAdapter.getItem(i)).getMyUid();
                            mPlayer2Name = Objects.requireNonNull(mMessageAdapter.getItem(i)).getName();

                            mDatabase.child("Users").child(mPlayer2Uid).child("player2Uid").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            mDatabase.child("Users").child(mPlayer2Uid).child("player2Name").setValue(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                            mDatabase.child("Users").child(mPlayer2Uid).child("status").setValue(UserGame.PLAYING);
                            runGame = true;
                            runGame(FirebaseAuth.getInstance().getCurrentUser().getUid(), mPlayer2Uid, Objects.requireNonNull(mMessageAdapter.getItem(i)).getName());
                            createGame();
//                            mDatabase.child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getMyUid()).child("uidPlayer2").setValue(mPlayer2Uid);
                        }
                    }
                }
            }
        });

        // Initialize progress bar
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);

//            updateGameUser();


//        mChildEventListener = new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
////                if (dataSnapshot != null){
////
////                    Intent myIntent = new Intent(SelectGamers.this, AndroidTicTacToeActivity.class);
////                    SelectGamers.this.startActivity(myIntent);
////                }
//
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        };
//
//        mGameDatabase.addChildEventListener(mChildEventListener);

        mValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DataSnapshot usersSnapshot = dataSnapshot.child("Users");
                Iterable<DataSnapshot> usersChildren = usersSnapshot.getChildren();

                mMessageAdapter.clear();
                countUsers = 0;

                for (DataSnapshot users : usersChildren) {
                    UserGame user = users.getValue(UserGame.class);
                    if (Objects.equals(Objects.requireNonNull(user).getMyUid(), Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                            && Objects.requireNonNull(user).getPlayer2Uid() != null){

                        mDatabase.child("Users").child(Objects.requireNonNull(user).getPlayer2Uid()).child("player2Uid").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        mDatabase.child("Users").child(Objects.requireNonNull(user).getPlayer2Uid()).child("status").setValue(UserGame.PLAYING);
                        runGame = true;
                        runGame(user.getPlayer2Uid(), user.getMyUid(), user.getPlayer2Name());
                        //Toast.makeText(getApplicationContext(), user.getMyUid() + " true " + mPlayer2Uid, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        //Toast.makeText(getApplicationContext(), user.getMyUid() + " false " + mPlayer2Uid, Toast.LENGTH_SHORT).show();
                    }
                    mMessageAdapter.add(user);
                    countUsers++;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //handle databaseError
            }
        };


        mDatabase.addValueEventListener(mValueEventListener);

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    // user is signed in
                    Toast.makeText(com.example.jfpinedap.androidtictactoe.SelectGamers.this, user.getDisplayName() + ", " + getString(R.string.sing_in_lable), Toast.LENGTH_SHORT).show();
                    onSignedInInitialize(user.getDisplayName());
                    updateGameUser();
                } else{
                    // user is signed out
                    onSignedOutCleanup();
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setAvailableProviders(Arrays.asList(
                                            new AuthUI.IdpConfig.EmailBuilder().build(),
                                            new AuthUI.IdpConfig.GoogleBuilder().build(),
                                            new AuthUI.IdpConfig.AnonymousBuilder().build()))
                                    .setLogo(R.drawable.icon_tictactoe)
                                    .setTheme(R.style.BlueTheme)
                                    .build(),
                            RC_SIGN_IN);
                }

            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_select_gamer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mDatabase.child("Users").child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).removeValue();
                AuthUI.getInstance().signOut(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mPlayer2Name", mPlayer2Name);
        outState.putString("mPlayer2Uid", mPlayer2Uid);
        outState.putInt("player1Score", player1Score);
        outState.putInt("player2Score", player2Score);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPlayer2Name = savedInstanceState.getString("mPlayer2Name");
        mPlayer2Uid = savedInstanceState.getString("mPlayer2Uid");
        player1Score = savedInstanceState.getInt("player1Score");
        player2Score = savedInstanceState.getInt("player2Score");
    }

    @Override
    protected void onStop() {
        super.onStop();

//        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//
//        if (currentFirebaseUser != null) {
//
//            mDatabase.child("Users").child(currentFirebaseUser.getMyUid()).child("status").setValue(UserGame.UNAVILABLE);
//        }

        // Save the current scores
        SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt("userScore", userScore);
        ed.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null) {
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
        detachDatabaseReadListener();
        mMessageAdapter.clear();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, R.string.sing_in_short_lable, Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, R.string.sing_in_canceled_lable, Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }

    private void updateGameUser() {

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();


        UserGame post = new UserGame(Objects.requireNonNull(currentFirebaseUser).getUid(), currentFirebaseUser.getDisplayName(), userScore , UserGame.AVILABLE, null, null);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(currentFirebaseUser.getUid(), postValues);

        mDatabase.child("Users").updateChildren(childUpdates);


        Toast.makeText(getApplicationContext(), "Update the user RTDB",
                Toast.LENGTH_SHORT).show();

    }

    private void onSignedInInitialize(String username) {
        mUsername = username;
        attachDatabaseReadListener();
    }


    private void onSignedOutCleanup() {
        mUsername = ANONYMOUS;
        mMessageAdapter.clear();
        detachDatabaseReadListener();
    }

    private void attachDatabaseReadListener() {
        if (mValueEventListener == null) {
            mValueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    DataSnapshot usersSnapshot = dataSnapshot.child("Users");
                    Iterable<DataSnapshot> usersChildren = usersSnapshot.getChildren();

                    mMessageAdapter.clear();
                    countUsers = 0;

                    for (DataSnapshot users : usersChildren) {
                        UserGame user = users.getValue(UserGame.class);
                        if (Objects.equals(Objects.requireNonNull(user).getMyUid(), Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid())
                                && Objects.requireNonNull(user).getPlayer2Uid() != null){

                            mDatabase.child("Users").child(Objects.requireNonNull(user).getPlayer2Uid()).child("player2Uid").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
                            mDatabase.child("Users").child(Objects.requireNonNull(user).getPlayer2Uid()).child("status").setValue(UserGame.PLAYING);
                            runGame = true;
                            runGame(user.getPlayer2Uid(), user.getMyUid(), user.getPlayer2Name());
                            //Toast.makeText(getApplicationContext(), user.getMyUid() + " true " + mPlayer2Uid, Toast.LENGTH_SHORT).show();
                        }
                        else{
                            //Toast.makeText(getApplicationContext(), user.getMyUid() + " false " + mPlayer2Uid, Toast.LENGTH_SHORT).show();
                        }
                        mMessageAdapter.add(user);
                        countUsers++;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //handle databaseError
                }
            };
            mDatabase.addValueEventListener(mValueEventListener);
        }

//            if (mChildEventListener == null) {
//                mChildEventListener = new ChildEventListener() {
//                    @Override
//                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                        if (dataSnapshot != null){
//                            Intent myIntent = new Intent(SelectGamers.this, AndroidTicTacToeActivity.class);
//                            SelectGamers.this.startActivity(myIntent);
//                        }
//                    }
//
//                    @Override
//                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                    }
//
//                    @Override
//                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//                    }
//
//                    @Override
//                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                };
//
//                mGameDatabase.addChildEventListener(mChildEventListener);
//            }
    }

    private void detachDatabaseReadListener() {
        if (mValueEventListener != null) {
            mDatabase.removeEventListener(mValueEventListener);
            mValueEventListener = null;
        }

//            if (mChildEventListener != null) {
//                mGameDatabase.removeEventListener(mChildEventListener);
//                mChildEventListener = null;
//            }
    }

    private void createGame(){
        mGameDatabase.child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).child("player_1").setValue(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mGameDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("player_2").setValue(mPlayer2Uid);
        mGameDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("player_1_Score").setValue("0");
        mGameDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("player_2_Score").setValue("0");
        mGameDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("tie_Score").setValue("0");
        mGameDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("board").setValue(null);
        mGameDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("turn").setValue(null);
    }

    private void runGame(String player1, String player2, String name){
        if (!runGame) {
            return;
        }
        Intent selectIntent = new Intent(SelectGamers.this, AndroidTicTacToeActivity.class);

        selectIntent.putExtra("player1Uid", player1);
        selectIntent.putExtra("player2Uid", player2);
        selectIntent.putExtra("opponentName", name);

        SelectGamers.this.startActivity(selectIntent);

        runGame = false;
    }
}
