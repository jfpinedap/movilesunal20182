package com.example.jfpinedap.androidtictactoe;

import java.util.HashMap;
import java.util.Map;

public class DataGame {
    
    private String player_1;
    private String player_2;
    private String player_1_Score;
    private String player_2_Score;
    private String tie_Score;
    private String board;
    private String turn;
    private String newGame;

    public DataGame() {
    }

    public DataGame(String player_1, String player_2, String player_1_Score, String player_2_Score, String tie_Score, String board, String turn, String newGame) {
        this.player_1 = player_1;
        this.player_2 = player_2;
        this.player_1_Score = player_1_Score;
        this.player_2_Score = player_2_Score;
        this.tie_Score = tie_Score;
        this.board = board;
        this.turn = turn;
        this.newGame = newGame;

    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("player_1", player_1);
        result.put("player_2", player_2);
        result.put("player_1_Score", player_1_Score);
        result.put("player_2_Score", player_2_Score);
        result.put("tie_Score", tie_Score);
        result.put("board", board);
        result.put("turn", turn);
        result.put("newGame", newGame);

        return result;
    }

    public String getBoard() {
        return board;
    }

    public char getTurn() {
        return turn.charAt(0);
    }

    public String getNewGame() {
        return newGame;
    }

    public String getPlayer_1_Score() {
        return player_1_Score;
    }

    public String getPlayer_2_Score() {
        return player_2_Score;
    }

    public String getPlayer_1() {
        return player_1;
    }
}
