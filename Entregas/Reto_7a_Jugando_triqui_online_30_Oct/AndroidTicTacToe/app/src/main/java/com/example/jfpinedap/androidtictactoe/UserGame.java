package com.example.jfpinedap.androidtictactoe;

import java.util.Map;
import java.util.HashMap;

public class UserGame {

    public static final String UNAVILABLE = "Unavailable";
    public static final String AVILABLE = "Available";
    public static final String PLAYING = "Playing";

    private String myUid;
    private String name;
    private int userScore = 0;
    private String player2Uid;
    private String player2Name;

    /** Current User status. */
    private String status = UNAVILABLE;

    public UserGame() {
    }

    public UserGame(String myUid, String name, int userScore, String status, String Player2Uid, String player2Name) {
        this.myUid = myUid;
        this.name = name;
        this.userScore = userScore;
        this.status = status;
        this.player2Uid = Player2Uid;
        this.player2Name = player2Name;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("myUid", myUid);
        result.put("name", name);
        result.put("userScore", userScore);
        result.put("status", status);
        result.put("player2Uid", player2Uid);
        result.put("player2Name", player2Name);


        return result;
    }

    public String getMyUid() {
        return myUid;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getPlayer2Uid() {
        return player2Uid;
    }

    public String getPlayer2Name() {
        return player2Name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserScore() {
        return "Score: " + String.valueOf(userScore);
    }

}
