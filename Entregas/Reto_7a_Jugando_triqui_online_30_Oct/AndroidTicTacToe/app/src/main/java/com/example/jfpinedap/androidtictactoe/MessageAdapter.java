package com.example.jfpinedap.androidtictactoe;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;

public class MessageAdapter extends ArrayAdapter<UserGame> {
    public MessageAdapter(Context context, int resource, List<UserGame> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.item_message, parent, false);
        }

        TextView messageTextView = (TextView) convertView.findViewById(R.id.messageTextView);
        TextView authorTextView = (TextView) convertView.findViewById(R.id.nameTextView);

        UserGame message = getItem(position);

        messageTextView.setVisibility(View.VISIBLE);
        authorTextView.setVisibility(View.VISIBLE);

        switch (Objects.requireNonNull(message).getStatus()){

            case UserGame.UNAVILABLE:
                authorTextView.setText(MessageFormat.format("{0} ---->  is Unavailable now.", ""));
                break;

            case UserGame.AVILABLE:
                authorTextView.setText(MessageFormat.format("{0} ---->  is Available now.", ""));
                break;

            case UserGame.PLAYING:
                authorTextView.setText(MessageFormat.format("{0} ---->  is Playing now.", ""));
                break;
//
//            default:
//                messageTextView.setText(MessageFormat.format("{0} ---->  is Unavailable now.", message.getUserScore()));
        }

        messageTextView.setText(message.getName());

        return convertView;
    }
}
