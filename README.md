Desarrollo de aplicaciones para dispositivos móviles
===================================================

Este repositorio está destinado para hacer la publicación de los trabajos personales para el curso Desarrollo de Aplicaciones para Dispositivos Móviles, Facultad de Ingeniería - Universidad Nacional de Colombia.

- Estudiante: Juan Francisco Pineda Pinzón
- Profesor: [Jorge E. Camargo, PhD](https://sites.google.com/site/camargoj/)
- Página del curso: [movilesunal20182](https://sites.google.com/site/movilesunal20182/)

Entregas
--------

A contiuación se encuentra el enlace hacia la carpeta que contiene los documentos entregables: 


Reto 0: [Hola Mundo en Android Studio [16 Ago]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_0_Hola_Mundo_en_Android_Studio_16_Ago)

Reto 1: [Propuesta proyecto [21 Ago]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_1_Propuesta_proyecto_21_Ago)

Reto 2: [Prototipo y Modelo Canvas [23 Ago]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_2_Prototipo_y_Modelo_Canvas_23_Ago)

Reto 3: [Juguemos Triqui [30 Ago]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_3_Juguemos_Triqui_30_Ago)

Reto 4: [Mejorando nuestro Triqui (Menus y Diálogos) [6 Sep]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_4_Mejorando_nuestro_Triqui_-_Menus_y_Diálogos_6_Sep)

Reto 5: [Incorporando gráficos y audio [18 Sep]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_5_Incorporando_gráficos_y_audio_18_Sep)

Reto 6: [Cambiando de orientación y guardando el estado [2 Oct]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_6_Cambiando_de_orientación_y_guardando_el_estado_2_Oct)

Reto 7: [Preferencias  [16 Oct]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_7_Preferencias__16_Oct)

Reto 7a: [Jugando triqui online [30 Oct]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_7a_Jugando_triqui_online_30_Oct)

Reto 8: [Usando SQLite [13 Nov]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_8_Usando_SQLite_13_Nov)

Reto 9: [Accediendo al GPS [20 Nov]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_9_Accediendo_al_GPS_20_Nov)

Reto 10: [Consumiendo webservices [27 Nov]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_10_Consumiendo_webservices_27_Nov)

Reto 11: [Procesando imágenes [4 Dic]](https://gitlab.com/jfpinedap/movilesunal20182/tree/master/Entregas/Reto_11_Procesando_imágenes_4_Dic)
